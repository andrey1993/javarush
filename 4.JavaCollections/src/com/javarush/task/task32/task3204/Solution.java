package com.javarush.task.task32.task3204;

import java.io.ByteArrayOutputStream;
import java.util.Random;

/* 
Генератор паролей
*/
public class Solution {
    public static void main(String[] args) {
        ByteArrayOutputStream password = getPassword();
        System.out.println(password.toString());
    }

    public static ByteArrayOutputStream getPassword() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Random random = new Random();
        int x = 8;
        int y = 1;
        try {
            while (x > 0) {
                int a = 1 + random.nextInt(x - 1);
                for (int i = 1; i <= a; i++) {
                    if (a > 6) a = -2;
                    int q = 65 + random.nextInt(91 - 65);
                    bytes.write(q);
                    x--;
                }
                if (x == 0) break;
                int b = 1 + random.nextInt(x - 1);
                for (int i = 1; i <= b; i++) {
                    if (x == b && y == 1) b--;
                    int q = 97 + random.nextInt(123 - 97);
                    bytes.write(q);
                    x--;
                }
                if (x == 0) break;
                int c = 1 + random.nextInt(x - 1);
                for (int i = 1; i <= c; i++) {
                    int q = 48 + random.nextInt(58 - 48);
                    bytes.write(q);
                    x--;
                }
                y++;
            }
        }catch (Exception e){
            int q = 48 + random.nextInt(58 - 48);
            bytes.write(q);
        }
        return bytes;
    }
}