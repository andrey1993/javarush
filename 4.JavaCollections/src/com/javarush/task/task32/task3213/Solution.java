package com.javarush.task.task32.task3213;

import java.io.IOException;
import java.io.StringReader;
import java.util.SortedSet;

/* 
Шифр Цезаря
*/

public class Solution {
    public static void main(String[] args) throws IOException {
//        StringReader reader = new StringReader("Khoor#Dpljr#&C,₷B'3");
        StringReader reader = null;
        System.out.println(decode(reader, -3));  //Hello Amigo #@)₴?$0
    }

    public static String decode(StringReader reader, int key) throws IOException {
        StringBuilder str = new StringBuilder();
        try {
            int x = reader.read();
            while (x > -1) {
                str.append((char) (x + key));
                x = reader.read();
            }
        } catch (NullPointerException e){
          //  e.printStackTrace();
        }
        return str.toString();
    }
}
