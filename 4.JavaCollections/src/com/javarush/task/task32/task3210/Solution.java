package com.javarush.task.task32.task3210;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/* 
Используем RandomAccessFile
*/

public class Solution {
    public static void main(String... args) {
        try {
            main2("D://isDirectory//dfg.txt", "5", "68");

//            RandomAccessFile raf = new RandomAccessFile(args[0], "rw");
//            byte[] bytes = args[2].getBytes();
//            raf.seek(Long.parseLong(args[1]));
//            raf.read(bytes, 0, args[2].getBytes().length);
//            String str = new String(bytes);
//            raf.seek(new File(args[0]).length());
//            if (args[2].equals(str)) raf.write("true".getBytes());
//            else raf.write("false".getBytes());
//            raf.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main2(String... args) throws IOException {
        RandomAccessFile raf = new RandomAccessFile(args[0], "rw");
        byte[] bytes = args[2].getBytes();
        raf.seek(Long.parseLong(args[1]));
        raf.read(bytes, 0, args[2].getBytes().length);
        String str = new String(bytes);
        raf.seek(raf.length());
        System.out.println(raf.length());
        System.out.println(new File(args[0]).length());
        if (args[2].equals(str)) raf.write("true".getBytes());
        else raf.write("false".getBytes());
        raf.close();
    }
}
