package com.javarush.task.task32.task3201;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/*
Запись в существующий файл
*/
public class Solution {
    public static void main(String... args) {
        try {
//            main2("D://isDirectory//dfg.txt", "5", "as");
            RandomAccessFile raf = new RandomAccessFile(new File(args[0]), "rw");
            if (raf.length() < Integer.parseInt(args[1])) raf.seek(raf.length());
            else raf.seek(Integer.parseInt(args[1]));
            raf.write(args[2].getBytes());
            raf.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//    public static void main2(String... args) throws IOException {
//            RandomAccessFile raf = new RandomAccessFile(new File(args[0]), "rw");
//            raf.seek(Integer.parseInt(args[1]));
//            raf.writeBytes(args[2]);
//            raf.close();
//
//
//    }
}

