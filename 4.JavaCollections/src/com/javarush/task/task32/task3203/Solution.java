package com.javarush.task.task32.task3203;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/*
Пишем стек-трейс
*/
public class Solution {
    public static void main(String[] args) {
        try {
            String text = getStackTrace(new IndexOutOfBoundsException("fff"));
            System.out.println(text);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getStackTrace(Throwable throwable) throws IOException {
        try (StringWriter strWrite = new StringWriter()) {
            PrintWriter printWriter = new PrintWriter(strWrite);
//            StackTraceElement[] stack = Thread.currentThread().getStackTrace();
            throwable.printStackTrace(printWriter);
//            for (StackTraceElement element : stack){
//                String str = element.toString();
//                strWrite.write(str);
//            }
            return strWrite.toString();
        }
    }
}