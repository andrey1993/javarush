package com.javarush.task.task31.task3102;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/* 
Находим все файлы
*/
public class Solution {
    public static List<String> getFileTree(String root) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        File[] files = new File(root).listFiles();
        Queue<File> filesQueue = new LinkedList<>();

        for (File file : files) filesQueue.offer(file);
        while (!filesQueue.isEmpty()) {
            File file = filesQueue.poll();
            if (file.isDirectory()) {
                for (File fileCheck : file.listFiles()) filesQueue.offer(fileCheck);
            } else {
                list.add(file.getAbsolutePath());
            }

        }
        return list;
    }

    public static void main(String[] args) {
        try {
            ArrayList<String> list = (ArrayList<String>) getFileTree("D://gg");
            for (String s : list) System.out.println(s);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
