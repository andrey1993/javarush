package com.javarush.task.task31.task3101;

import java.io.*;
import java.util.ArrayList;

/*
Проход по дереву файлов
*/
public class Solution {
    public static void main(String[] args) {
        try {

            File file1 = new File(args[0]);
            //  File file1 = new File( "D://gg");
            File[] files = file1.listFiles();
            ArrayList<File> list = ckeckFolder(files, new ArrayList<>());
            list.sort((o1, o2) -> {
                String name1 = o1.getName();
                String name2 = o2.getName();
                int x = name1.compareTo(name2);
                return x;
            });
            File file2 = new File(args[1]);
            StringBuilder str = new StringBuilder(file2.getParent()).append("/allFilesContent.txt");
            File file3 = new File(str.toString());
            FileUtils.renameFile(file2, file3);
            OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file3));
            //         OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream("D://gg//1//1.1//ggfin.txt"));
            for (File file : list) {
                InputStreamReader reader = new InputStreamReader(new FileInputStream(file));
                while (reader.ready()) {
                    int x = reader.read();
                    writer.write(x);
                }
                writer.write("\n");
                reader.close();
            }
            writer.flush();
            writer.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
        //  for (File file : list) System.out.println(file.getName());
    }

    public static ArrayList<File> ckeckFolder(File[] files, ArrayList<File> list) {
        for (File file : files) {
            if (file.isDirectory()) {
                ckeckFolder(file.listFiles(), list);
            } else {
                if (file.length() <= 50) list.add(file);  //System.out.println(file.length());
            }
        }
        return list;
    }
}
