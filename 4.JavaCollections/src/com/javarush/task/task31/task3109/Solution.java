package com.javarush.task.task31.task3109;

import java.io.*;
import java.util.Properties;

/* 
Читаем конфиги
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        Solution solution = new Solution();
        Properties properties = solution.getProperties("4.JavaCollections/src/com/javarush/task/task31/task3109/properties.xml");
        properties.list(System.out);

        properties = solution.getProperties("4.JavaCollections/src/com/javarush/task/task31/task3109/properties.txt");
        properties.list(System.out);

        properties = solution.getProperties("4.JavaCollections/src/com/javarush/task/task31/task3109/notExists");
        properties.list(System.out);
    }

    public Properties getProperties(String fileName) {
        Properties properties = new Properties();
        try (InputStream inputStream = new FileInputStream(fileName)) {
            properties.loadFromXML(inputStream);
            return properties;
        } catch (Exception e) {
            try {
                Reader reader = new FileReader(fileName);
                properties.load(reader);
                reader.close();
                return properties;
            } catch (Exception e1) {
                return properties;
            }

        }
    }
}
