package com.javarush.task.task31.task3105;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/* 
Добавление файла в архив
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        Map<String, byte[]> map = new LinkedHashMap<>();

//        Path file = Paths.get("D://sdfsf.msi");
        Path file = Paths.get("D://isDirectory//isfiles.txt");
        ZipInputStream readZip = new ZipInputStream(new FileInputStream("D://isDirectory//WinRAR.zip"));
//        Path file = Paths.get(args[0]);
//        ZipInputStream readZip = new ZipInputStream(new FileInputStream(args[1]));
        ZipEntry zipEntry = readZip.getNextEntry();
        while (zipEntry != null) {
            ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
            int x;
            byte[] bytes = new byte[10000];
            while ((x = readZip.read(bytes)) > 0) {
                byteArray.write(bytes, 0, x);
            }
            byteArray.close();
            if (!(zipEntry.getName().equals("new/" + file.getFileName()))) {
                map.put(zipEntry.getName(), byteArray.toByteArray());
            }
            readZip.closeEntry();
            zipEntry = readZip.getNextEntry();
        }
        readZip.close();

        ZipOutputStream zipAdd = new ZipOutputStream(new FileOutputStream("D://isDirectory//WinRAR.zip"));
//        ZipOutputStream zipAdd = new ZipOutputStream(new FileOutputStream(args[1]));
        ZipEntry zipEntry1 = new ZipEntry("new/" + file.getFileName());
        zipAdd.putNextEntry(zipEntry1);
        Files.copy(file, zipAdd);

        for (Map.Entry<String, byte[]> pair : map.entrySet()) {
            zipAdd.putNextEntry(new ZipEntry(pair.getKey()));
            zipAdd.write(pair.getValue());
            zipAdd.closeEntry();
        }


        zipAdd.close();

    }
}
