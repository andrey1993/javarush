package com.javarush.task.task31.task3112;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/* 
Загрузчик файлов
*/
public class Solution {

    public static void main(String[] args) throws IOException {
        Path passwords = downloadFile("https://javarush.ru/testdata/secretPasswords.txt", Paths.get("D:/MyDownloads"));

        for (String line : Files.readAllLines(passwords)) {
            System.out.println(line);
        }
    }

    public static Path downloadFile(String urlString, Path downloadDirectory) throws IOException {
        URL url = new URL(urlString);
        Path file = null;
        try (InputStream read = url.openStream()) {
            String filename = urlString.substring(urlString.lastIndexOf("/") + 1);
            Path tempfile = Files.createTempFile("pre-", ".tmp");
            Files.copy(read, tempfile, StandardCopyOption.REPLACE_EXISTING);
            file = Paths.get(downloadDirectory + "/" + filename);
            if (!Files.exists(downloadDirectory)){
                Files.createDirectory(downloadDirectory);
            }
            Files.move(tempfile, file, StandardCopyOption.REPLACE_EXISTING);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;
    }
}
