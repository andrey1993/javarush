package com.javarush.task.task31.task3106;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


/*
Разархивируем файл
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        Vector<FileInputStream> list = new Vector<>();
//        for (int i = 1;i < args.length; i++) list.add(args[i]);
        Path path1 = Paths.get("D://isDirectory//WinRAR1.zip");
        Path path2 = Paths.get("D://isDirectory//WinRAR2.zip");
        Path path3 = Paths.get("D://isDirectory//WinRAR3.zip");
        list.add(new FileInputStream(path1.toString()));
        list.add(new FileInputStream(path2.toString()));
        list.add(new FileInputStream(path3.toString()));
   //     Collections.sort(list);
        ZipInputStream readFiles = new ZipInputStream(list.get(1));
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        ZipEntry zipEntry = readFiles.getNextEntry();
        int x = 0;
        while (zipEntry != null) {
            byte[] bytes = new byte[100000];
            while ((x = readFiles.read(bytes)) > 0) {
                byteArray.write(bytes, 0, x);
            }
            zipEntry = readFiles.getNextEntry();
//            if (zipEntry == null) {
//                if (list.size()>0) {
//                    readFiles.close();
//                    readFiles = new ZipInputStream(new FileInputStream(list.poll()));
//                    zipEntry = readFiles.getNextEntry();
//                }
//            }
        }
        readFiles.close();
        byteArray.close();

//        FileOutputStream write = new FileOutputStream(args[0]);
        FileOutputStream write = new FileOutputStream("D://isDirectory//poper.txt");
        write.write(byteArray.toByteArray());
        write.close();

    }
}