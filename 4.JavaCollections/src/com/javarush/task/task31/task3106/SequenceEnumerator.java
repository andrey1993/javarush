package com.javarush.task.task31.task3106;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class SequenceEnumerator implements Enumeration<FileInputStream> {
    Enumeration<FileInputStream> list;

    public SequenceEnumerator(Vector<FileInputStream> list) {
        this.list = list.elements();
    }

    @Override
    public boolean hasMoreElements() {
        return list.hasMoreElements();
    }

    @Override
    public FileInputStream nextElement() {
            return list.nextElement();
    }
}
