package com.javarush.task.task31.task3113;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/* 
Что внутри папки?
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader readConsole = new BufferedReader(new InputStreamReader(System.in));
        Path objectInDirectory = Paths.get(readConsole.readLine());
        readConsole.close();
        int[] countDir = {0};
        int[] countFile = {0};
        long[] size = {0};
        class MyFileVisitor extends SimpleFileVisitor {
            @Override
            public FileVisitResult preVisitDirectory(Object dir, BasicFileAttributes attrs) throws IOException {
                countDir[0]++;
                return super.preVisitDirectory(dir, attrs);
            }

            @Override
            public FileVisitResult visitFile(Object file, BasicFileAttributes attrs) throws IOException {
                countFile[0]++;
                size[0] += Files.size((Path) file);
                return super.visitFile(file, attrs);
            }
        }
        MyFileVisitor visitor = new MyFileVisitor();

        if (!Files.isDirectory(objectInDirectory)) System.out.println(objectInDirectory + " - не папка");
        else {
            Files.walkFileTree(objectInDirectory, visitor);
        }
        countDir[0]--;

        System.out.println("Всего папок - " + countDir[0]);
        System.out.println("Всего файлов - " + countFile[0]);
        System.out.println("Общий размер - " + size[0]);
    }
}
