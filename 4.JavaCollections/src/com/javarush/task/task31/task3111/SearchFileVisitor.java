package com.javarush.task.task31.task3111;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class SearchFileVisitor extends SimpleFileVisitor<Path> {
    private String partOfName, partOfContent;
    private int minSize, maxSize;
    private List<Path> foundFiles = new ArrayList<>();

    public List<Path> getFoundFiles() {
        return foundFiles;
    }

    public String getPartOfName() {
        return partOfName;
    }

    public void setPartOfName(String partOfName) {
        this.partOfName = partOfName;
    }

    public String getPartOfContent() {
        return partOfContent;
    }

    public void setPartOfContent(String partOfContent) {
        this.partOfContent = partOfContent;
    }

    public int getMinSize() {
        return minSize;
    }

    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }


    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        byte[] content = Files.readAllBytes(file); // размер файла: content.length
        String str = new String(content);
        ArrayList<String> search = new ArrayList<>();
        if (partOfName != null) search.add("partOfName");
        if (partOfContent != null) search.add("partOfContent");
        if (maxSize != 0) search.add("maxSize");
        if (minSize != 0) search.add("minSize");
        for (String strx : search) {
            if (strx.equals("partOfName")) {
                if (!(file.getFileName().toString().contains(partOfName))) return super.visitFile(file, attrs);
            }
            if (strx.equals("partOfContent")) {
                if (!(str.contains(partOfContent))) return super.visitFile(file, attrs);
            }
            if (strx.equals("maxSize")){
                if (!(content.length < maxSize)) return super.visitFile(file, attrs);
            }
            if (strx.equals("minSize")){
                if (!(content.length > minSize)) return super.visitFile(file, attrs);
            }
        }
        foundFiles.add(file);


        return super.visitFile(file, attrs);
    }
}
