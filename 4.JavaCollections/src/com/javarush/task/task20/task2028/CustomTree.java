package com.javarush.task.task20.task2028;

import java.io.Serializable;
import java.util.*;


/* 
Построй дерево(1)
*/
public class CustomTree extends AbstractList<String> implements Cloneable, Serializable {

    ArrayList<Entry> list = new ArrayList<>();
    int i = 1;

    Entry<String> root;

    CustomTree() {
        root = new Entry<>("root");
        list.add(root);
    }

    @Override
    public String get(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return list.size() - 1;
    }

    @Override
    public String set(int index, String element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, String element) {
        throw new UnsupportedOperationException();
    }

//    @Override
//    public String remove(int index) {
//        throw new UnsupportedOperationException();
//    }

    @Override
    public boolean remove(Object o) {
        if (!(o instanceof String)) throw new UnsupportedOperationException();
        final String str = o.toString();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).elementName.equals(o)) {
                if (list.get(i).leftChild != null) remove(list.get(i).leftChild.elementName);
                if (list.get(i).rightChild != null) remove(list.get(i).rightChild.elementName);
                this.i = 0;
                root = list.get(this.i);
                //    System.out.println(i);
                list.remove(i);
            }
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends String> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<String> listIterator(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    public String getParent(String name) {
        for (Entry entry : list) {
            if (entry.elementName.equals(name)) return entry.parent.elementName;
        }
        return null;
    }

    public boolean add(String str) {
        root.checkChildren();
//        System.out.println(root.elementName);
        if (root.isAvailableToAddChildren()) {
            if (root.availableToAddLeftChildren) {
                root.leftChild = new Entry<>(str);
                root.leftChild.parent = root;
                list.add(root.leftChild);
            } else if (root.availableToAddRightChildren) {
                root.rightChild = new Entry<>(str);
                root.rightChild.parent = root;
                list.add(root.rightChild);
            }
        } else {
            if (i == list.size()) {
                i--;
                int b = i / 2;
                while (i >= b) {
                    root = list.get(i);
                    root.leftChild = null;
                    root.rightChild = null;
                    root.addCheckChildren();
                    i--;
                }
                i++;
            }
            root = list.get(i);
            i++;
            add(str);
        }
        return true;
    }

    static class Entry<T> implements Serializable {
        String elementName;
        int lineNumber;
        boolean availableToAddLeftChildren, availableToAddRightChildren;
        Entry<T> parent, leftChild, rightChild;

        public Entry(String str) {
            elementName = str;
            availableToAddLeftChildren = true;
            availableToAddRightChildren = true;
        }

        void checkChildren() {
            if (leftChild != null) availableToAddLeftChildren = false;
            if (rightChild != null) availableToAddRightChildren = false;
        }
        void addCheckChildren() {
            if (leftChild == null) availableToAddLeftChildren = true;
            if (rightChild == null) availableToAddRightChildren = true;
        }

        boolean isAvailableToAddChildren() {
            boolean a = (availableToAddLeftChildren || availableToAddRightChildren);
            return a;
        }
    }
}
