package com.javarush.task.task22.task2207;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/* 
Обращенные слова
*/
public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void main(String[] args) {
        BufferedReader readConsole = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> words = new ArrayList<>();
        try {
             BufferedReader readFile = new BufferedReader(new FileReader(readConsole.readLine()));
          //  BufferedReader readFile = new BufferedReader(new FileReader("D://gg.txt"));
            String str;
            while ((str = readFile.readLine()) != null) {
                str = str.replaceAll("\uFEFF", "");
                String[] strings = str.split(" ");
                Collections.addAll(words, strings);
            }
            for (int i = 0; i < words.size(); i++) {
                String word = words.get(i);
                StringBuilder wordReverse = new StringBuilder(word).reverse();
                words.remove(i);
                if (words.contains(wordReverse.toString())){
                    Pair pair1 = new Pair();
                    pair1.first = word;
                    pair1.second = wordReverse.toString();
                    result.add(pair1);
                }
            }
            for (Pair pair : result) System.out.println(pair.first + " " + pair.second);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class Pair {
        String first;
        String second;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pair pair = (Pair) o;

            if (first != null ? !first.equals(pair.first) : pair.first != null) return false;
            return second != null ? second.equals(pair.second) : pair.second == null;

        }

        @Override
        public int hashCode() {
            int result = first != null ? first.hashCode() : 0;
            result = 31 * result + (second != null ? second.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return first == null && second == null ? "" :
                    first == null && second != null ? second :
                            second == null && first != null ? first :
                                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;

        }
    }

}
