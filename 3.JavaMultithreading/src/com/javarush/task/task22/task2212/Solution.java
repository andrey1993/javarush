package com.javarush.task.task22.task2212;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Проверка номера телефона
*/
public class Solution {
    public static boolean checkTelNumber(String telNumber) {
        if (telNumber == null) return false;
        Pattern pattern = Pattern.compile("\\d");
        Matcher matcher = pattern.matcher(telNumber);
        int q = 0;
        while (matcher.find()) {
            q++;
        }
        if (q == 10) {
        boolean m = Pattern.matches("^(\\d+)?(\\(\\d{3}\\))?\\d+(-\\d+){0,2}",telNumber);
        if (m)return m;
        }
        if (q == 12) {
            boolean m = Pattern.matches("\\+(\\d+)?(\\(\\d{3}\\))?\\d+(-\\d+){0,2}\\d+",telNumber);
            if (m)return m;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(checkTelNumber("+(145)41-65-89231"));
    }
}
