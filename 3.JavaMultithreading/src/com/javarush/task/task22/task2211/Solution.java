package com.javarush.task.task22.task2211;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Arrays;

/* 
Смена кодировки
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        Charset windows1251 = Charset.forName("Windows-1251");
        Charset UTF_8 = Charset.forName("UTF-8");
        InputStreamReader readFile =new  InputStreamReader(new FileInputStream(args[0]), windows1251);
        char[] chars = new char[1000];
        readFile.read(chars);
        readFile.close();
        OutputStreamWriter writeFile = new OutputStreamWriter(new FileOutputStream(args[1]), UTF_8);
        writeFile.write(chars);
        writeFile.close();

    }
}
