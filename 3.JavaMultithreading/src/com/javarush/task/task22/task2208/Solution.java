package com.javarush.task.task22.task2208;

import java.util.HashMap;
import java.util.Map;

/* 
Формируем WHERE
*/
public class Solution {
    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap<>();
        map.put(null, null);
        map.put("name", "Ivanov");
        map.put("country", "Ukraine");
        map.put("city", "Kiev");
        map.put("age", null);
        System.out.println(getQuery(map));
    }

    public static String getQuery(Map<String, String> params) {
        StringBuilder str = new StringBuilder();
        for (Map.Entry<String, String> parametr : params.entrySet()) {
            String key = parametr.getKey();
            String value = parametr.getValue();
            if (key == null || value == null) {
                str.append("");
                continue;
            }
            str.append(key).append(" = '").append(value).append("'");
            str.append(" and ");
        }
        if (str.length() <= 5) {
            return str.toString();
        } else return str.substring(0, str.length() - 5);
    }
}
