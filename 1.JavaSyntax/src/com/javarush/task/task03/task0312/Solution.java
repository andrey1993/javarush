package com.javarush.task.task03.task0312;

/* 
Конвертируем время
*/

public class Solution {

    public static void main(String[] args) {
        int x = 1;
        int y = 1;

        while (x <= 10) {
            while (y <= 10) {
                System.out.print(y*x+" ");
                y++;
            }
            y=1;
            System.out.println();
            x++;
        }



    }
}
